# 1. About URL Shortener
URL Shortener is the program to convert from long string URL to the shorter form. The purpose of converting to the shorter one is because it will help us able to share the URLs, copying and pasting very easily.

* The production version at [https://shortener.wintech-thai.com/](https://shortener.wintech-thai.com/)
* The development version is at [https://sandbox.wintech-thai.com/laravel/shortener/public/](https://sandbox.wintech-thai.com/laravel/shortener/public/)
 
# 2. Installation

In order to install the application we will be assuming that the PostgreSQL, Apache web Server, Laravel and PHP are already in place. Currently on the development environment is using PostgreSQL version 10.4, Apache Web Server version 2.4.6, Laravel version 5.6.26 and PHP version is 7.2.6.

 
* Your Laravel setup should start from /var/www/html

* Set the Apache DocumentRoot to this path /var/www/html/url_shortener/public, the "url_shortener" is the location that our URL Shortener is kept.
 
* Copy the our code from Bitbucket to replace the ones in the path mentioned earlier. It is a good idea to change Laravel to maintenance mode before we do this step.  

* Run the migration to apply the new database changes.

Actuall, my intention was to create the youtube video clip about the installation steps but this can be in the future if time available.

# 3. Things to improve

* Add more unit test cases for URLController.php and BlacklistController.php. Based the current availability, I can just select for some cases to implement them but they are good enough to show the ideas of how we use PHPUnit in the project.
 
* Adopt the TDD concept, I started this project by creating the code first and then created the unit test cases.  To follow the TDD, we will have to think what we are going to develop and how to test them first and then create the test and then put the logic to the code and test, the all results of the testing need to be passed.
 
* Refactor some part of code. Laravel has so many built-in functions that we can use without reinventing the wheel. Doing this will help our code looks cleaner and maintainable. The refactoring won't be the difficult task anymore because we already have the PHPUnit to help us verify and make sure that our code changes won't break the logic. 
 
* Change the routing to be more RESTful standard. 

* Develop a build script to create a tar.gz installation package. This script will need to pull the source codes from git. We should be able to specify the tag/version of our code that we are going to build. We should also keep the configuration files such as .env for production in another git repository and need to be authorized to pull the files from this repository, this will help to prevent production configuration to be seen by developers.

* Develop a deployment script to extract .tar.gz generated from above to the deployment directory. We can also use this script to perform the other activities such as start/stop Laravel, configure for the config and routing cache and then perform the migration tasks.  