<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

//Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

//Actually, any methods for blacklists should be filter by the 'auth' middleware
Route::get('/blacklists', 'BlacklistController@index')->name('blacklists');
Route::get('/blacklists/create', 'BlacklistController@create')->name('create_blacklist');
Route::post('/blacklists/store', 'BlacklistController@store')->name('store_blacklist');
Route::get('/blacklists/edit/{id}', 'BlacklistController@edit')->name('edit_blacklist');
Route::post('/blacklists/update/{id}', 'BlacklistController@update')->name('update_blacklist');
Route::delete('/blacklists', 'BlacklistController@destroy')->name('delete_blacklist');

Route::get('/urls', 'UrlController@index')->name('urls')->middleware('auth');
Route::delete('/urls', 'UrlController@remove')->name('delete_urls')->middleware('auth');

//Actuall we can use /urls instead but I just wanted to try to use only "url" (without s)
Route::get('/url', 'UrlController@create')->name('url_entry');
Route::post('/url', 'UrlController@shorten');
Route::get('/', 'UrlController@create');

Route::get('/{key}', 'UrlController@redirect');

?>
