@extends('home', ['tab_mode' => 'blacklist'])

@section('tab_content')

<!-- Start : tab_url.blade.php -->
<div id="url">

<br/>
<div>
  <div class="form-check-inline">
    <form class="form-inline" action="{{ route('blacklists') }}">
      <input type="text" class="form-control mb-2 mr-sm-2" id="url" name="search_text" value="{{ $search_text }}"/>
      <button type="submit" class="btn text-white bg-secondary mb-2 mr-sm-2">SEARCH</button>
      <span>Maximum regx allow is 5</span>
    </form>
  </div>
</div>

  <form action="{{ route('delete_blacklist') }}" method="POST">

  <table class="table table-bordered">
    <col width="5%"/>
    <col width="40%"/>
    <col width="50%"/>
    <col width="5%"/>
    <thead class="thead-light">
      <tr>
        <th class="text-center"><i class="material-icons" style="color: blue">delete_outline</i></th>
        <th>Black List Regx</th>
        <th>Description</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($blacklists as $blacklist)
      <tr>
        <td align="center"><input type="checkbox" value="{{ $blacklist->id }}" name="cbxDelete[]"/></td>
        <td>{{ $blacklist->pattern }}</td>
        <td>{{ $blacklist->description }}</td>
        <td>
          <button type="submit" formmethod="get" formaction="{{ route('edit_blacklist', $blacklist->id) }}" class="btn btn-default btn-sm">
            EDIT 
          </button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  @if ($errors->has('blacklist_count'))
  <div class="alert alert-danger" role="alert">
    <strong>{{ $errors->first('blacklist_count') }}</strong>
  </div>
  @endif

    @CSRF

    {{ method_field('DELETE') }}
    <button type="submit" formmethod="get" formaction="{{ route('create_blacklist') }}" class="btn text-white bg-secondary mb-2 mr-sm-2">ADD</button>
    <button type="submit" class="btn text-white bg-secondary mb-2 mr-sm-2">DELETE</button>
  </form>

</div>

<!-- End : tab_url.blade.php -->
@endsection
