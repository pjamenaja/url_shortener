<!DOCTYPE html>

<!-- Start : app.blade.php -->
<html lang="en">
<head>
  <title>URL Shorten by Seubpong</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <form method="get" action="{{ route('urls') }}">
      <button class="btn btn-danger navbar-btn pull-right btn-space">LOGIN</button>
    </form>
  </div>
</nav>

@yield('content')

</body>
</html>
<!-- End : app.blade.php -->
