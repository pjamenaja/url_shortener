@extends('app')

@section('content')

<div class="container">
  <h2>Please put your long URL and press 'SHORTEN' button</h2>

  @if($error_flag) 
    <div class="alert alert-danger">
      <strong>Error!</strong> {{ $error_msg }}
    </div>
  @endif

  <form action="{{ route('url_entry') }}" method="post">
    <div class="form-group">
      @csrf

      <label for="long_url">LONG URL:</label>
      <input type="text" name="long_url" class="form-control" id="long_url" value="{{ $long_url }}">
    </div>

    <div class="radio">
      <label><input value="0" checked type="radio" name="optDay" class="radio-inline">NO EXPIRE DATE</label>
      <label><input value="1" type="radio" name="optDay" class="radio-inline">EXPIRE IN 1 DAY</label>
      <label><input value="5" type="radio" name="optDay" class="radio-inline">EXPIRE IN 5 DAYS</label>
      <label><input value="30" type="radio" name="optDay" class="radio-inline">EXPIRE IN 30 DAYS</label>
    </div>

    <button type="submit" class="btn btn-primary btn-lg btn-block">SHORTEN</button>
    <br/>
  </form>
</div>

<div class="container">
  <div class="form-group">
    <label for="short_url">SHORTEN URL:</label>
    <input type="text" class="form-control" id="short_url" disabled="true" value="{{ $shorten_url }}">
  </div>
</div>

@endsection
