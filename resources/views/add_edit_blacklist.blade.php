@extends('home', ['tab_mode' => 'blacklist'])

@section('tab_content')
<!-- Start : add_edit_blacklist.blade.php -->

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                @if($mode == 'add')
                  Add regular expression
                @else
                  Edit regular expression
                @endif
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('store_blacklist') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Regular Expression</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="pattern" value="{{ $blacklist->pattern }}" autofocus>
                                @if ($errors->has('pattern'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('pattern') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="desc" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <input id="desc" class="form-control" type="text" name="desc" value="{{ $blacklist->description }}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                @if($mode == 'add')
                                    <button type="submit" class="btn text-white bg-secondary">ADD</button>
                                @else
                                    <button type="submit" formmethod="post" formaction="{{ route('update_blacklist', $blacklist) }}" class="btn text-white bg-secondary">UPDATE</button>
                                @endif
                                <button type="submit" formmethod="get" formaction="{{ route('blacklists') }}" class="btn text-white bg-secondary">CANCEL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End : add_edit_blacklist.blade.php -->
@endsection
