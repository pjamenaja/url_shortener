@extends('home', ['tab_mode' => 'url'])

@section('tab_content')

<!-- Start : tab_url.blade.php -->
<div id="url"><br/>

<div>

  <div class="form-check-inline">
    <form class="form-inline" action="{{ route('urls') }}">
      <input type="text" class="form-control mb-2 mr-sm-2" id="url" name="search_text" value="{{ $search_text }}"/>
      <button type="submit" class="btn text-white bg-secondary mb-2 mr-sm-2">SEARCH</button>

     <div class="form-group">
       <label class="mb-2 mr-sm-2" for="sel1">Go to</label>
       <select class="form-control mb-2 mr-sm-2" id="page" name="page">
         @for ($i = 1; $i<=$page_count; $i++)
           @if($i == $curr_page)
             <option selected="true">{{ $i }}</option>
           @else
             <option>{{ $i }}</option>
           @endif
         @endfor
       </select>
       <button type="submit" class="btn text-white bg-secondary mb-2 mr-sm-2">GO</button>

    </form>
    <span>Total : {{ $total_record }}</span>
  </div>

</div>

  <form action="{{ route('delete_urls') }}" method="POST">

  <table class="table table-bordered">
    <col width="5%"/>
    <col width="5%"/>
    <col width="20%"/>
    <col width="10%"/>
    <col width="12%"/>
    <col width="48%"/>
    <thead class="thead-light">
      <tr>
        <th class="text-center"></th>
        <th class="text-center"><i class="material-icons" style="color: blue">delete_outline</i></th>
        <th>Short URL</th>
        <th>Hit Count</th>
        <th>Expire Date</th>
        <th>Long URL</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($urls as $url)
      <tr style="text-overflow: ellipsis;">
        @if($url->isExpire)
          <td align="center"><i class="material-icons" style="color: red">alarm_off</i></td>
        @elseif($url->isInBlacklist)
          <td align="center"><i class="material-icons" style="color: red">report_off</i></td>
        @else
          <td align="center"></td>
        @endif
        <td align="center"><input type="checkbox" value="{{ $url->id }}" name="cbxDelete[]"/></td>
        <td>
          <a target="_blank" href="{{ $base_url . '/' . $url->short_url }}">{{ $url->short_url }}</a>
        </td>
        <td align="center">{{ number_format($url->hit_count) }}</td>
        <td>{{ substr($url->expire_date, 0, 10) }}</td>
        <td>
          <a target="_blank" href="{{ $url->long_url }}">{{ substr($url->long_url, 0, 50) }}...</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

    @CSRF

    {{ method_field('DELETE') }}
    <button type="submit" class="btn text-white bg-secondary mb-2 mr-sm-2">DELETE</button>
  </form>

</div>

<!-- End : tab_url.blade.php -->
@endsection
