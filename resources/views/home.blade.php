@extends('layouts.app')

@section('content')
<!-- Start : home.blade.php -->
<div class="container-fluid">

  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      @if($tab_mode == 'url')
        <a class="nav-link active" href="{{ route('urls') }}">URLs</a>
      @else
        <a class="nav-link" href="{{ route('urls') }}">URLs</a>
      @endif
    </li>

    <li class="nav-item">
      @if($tab_mode == 'blacklist')
        <a class="nav-link active" href="{{ route('blacklists') }}">Black List</a>
      @else
        <a class="nav-link" href="{{ route('blacklists') }}">Black List</a>
      @endif
    </li>
  </ul>

  @yield('tab_content')

<!-- End : home.blade.php -->
</div>

@endsection
