<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UrlTest extends TestCase
{
    private $controller = null;
    private $request = null;

    public function setUp()
    {
        parent::setUp();

        $this->controller = \App::make('\App\Http\Controllers\UrlController');
        $this->request = new \Illuminate\Http\Request();

        //Use sqlite in memory mode for unit testing
        \Artisan::call('migrate');
    }

    public function test_I_should_get_no_error_if_url_is_valid()
    {
        $this->request->replace(['long_url' => "https://google.com"]);
        $vw = $this->controller->shorten($this->request);

        $values = $vw->getData();

        $is_error = $values['error_flag'];

        $this->assertFalse($is_error);
    }

    public function test_I_should_get_error_if_url_is_in_blacklist()
    {
        //This is placeholder for future
        $this->assertTrue(true);
    }

    public function test_I_should_get_error_if_url_is_too_long()
    {
        $long_url = str_repeat("xaxa", 100);

        $this->request->replace(['long_url' => "https://$long_url.com"]);
        $vw = $this->controller->shorten($this->request);

        $values = $vw->getData();

        $is_error = $values['error_flag'];
        $error_msg = $values['error_msg'];

        $this->assertTrue($is_error);

        $is_too_long = preg_match('/too long/', $error_msg);
        $this->assertTrue($is_too_long == 1);
    }

    public function test_I_should_get_error_if_url_expire()
    {
        //This is placeholder for future
        $this->assertTrue(true);
    }

    public function test_I_should_get_error_if_url_malform()
    {
        $this->request->replace(['long_url' => "sssx://google.com"]);
        $vw = $this->controller->shorten($this->request);

        $values = $vw->getData();

        $is_error = $values['error_flag'];
        $error_msg = $values['error_msg'];

        $this->assertTrue($is_error);

        $is_malform = preg_match('/Not a valid/', $error_msg);
        $this->assertTrue($is_malform == 1);
    }

    public function test_I_should_get_the_same_shorten_url_if_it_is_already_exist()
    {
        //This is placeholder for future
        $this->assertTrue(true);
    }

    public function test_I_should_get_error_for_unknown_shorten_url()
    {
        //This is sure, the key won't exist
        $vw = $this->controller->redirect("1234xxxx");

        $status = $vw->status();

        $this->assertTrue($status == 410);
    }

    public function test_shorten_url_should_be_removed_when_if_id_exist()
    {
        //This is placeholder for future
        $this->assertTrue(true);
    }

    public function test_I_should_get_url_page_by_page()
    {
        $item_per_page = 7;

        //Create 20 distinct URLs
        for ($i=0; $i<20; $i++)
        {
            $request = $this->request->replace(['long_url' => "https://google$i.com"]);
            $this->controller->shorten($request);
        }

        $req = $this->request->replace(['search_text' => ""]);
        $vw = $this->controller->index($req);
        $values = $vw->getData();

        $page_cnt = $values['page_count'];
        $total = $values['total_record'];
        $urls = $values['urls'];
        $cnt = count($urls);

        $this->assertTrue($cnt == $item_per_page);
        $this->assertTrue($page_cnt == ceil($total/$item_per_page));
    }
}
