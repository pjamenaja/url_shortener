<?php

namespace App\Http\Controllers;

use App\Blacklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class BlacklistController extends Controller
{
    private const ITEM_PER_PAGE = 7;
    private const MAX_BLACKLIST_ALLOW = 5;
    private $m = null;

    public function __construct(Blacklist $url)
    {
        //TODO : We may use Interface to inject into the constructor to make our program be more flexible

        $this->m = $url;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search_text');

        $m = $this->m->where('pattern', 'LIKE', '%'.$search.'%')
                     ->orderBy('id', 'DESC');

        $blacklists = $m->paginate($this::ITEM_PER_PAGE);

        $tot = $blacklists->total();
        $cnt = ceil($tot/$this::ITEM_PER_PAGE);
        $pageNo = $blacklists->currentPage();

        $params = [
                      'blacklists' => $blacklists,
                      'page_count' => $cnt,
                      'total_record' => $tot,
                      'curr_page' => $pageNo,
                      'search_text' => $search,
                  ];

        return(view('tab_blacklist', $params));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Support\MessageBag $mbag;
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, MessageBag $mbag)
    {
        $obj = $this->m;

        //Check if blacklist count is greater than the limit
        $total = $obj->count();

        if ($total >= $this::MAX_BLACKLIST_ALLOW)
        {
            $msg = sprintf("Number of blacklist is greater than %s", $this::MAX_BLACKLIST_ALLOW);
            $mbag->add('blacklist_count', $msg);

            return($this->index($request)->withErrors($mbag));
        }

        $params = [
                      'mode' => 'add',
                      'blacklist' => $obj,
                  ];

        return(view('add_edit_blacklist', $params));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pattern = $request->input('pattern');
        $desc = $request->input('desc');

        $blstObj = $this->m;

        $blstObj->pattern = $pattern;
        $blstObj->description = $desc;

        $this->validateRequest('store', 0, $request);

        $blstObj->save();
        return($this->index($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function show(Blacklist $blacklist)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $id)
    {
        $pattern = $request->input('pattern');
        $desc = $request->input('desc');

        $obj = $this->m->where('id', $id)->first();
        if (!isset($obj))
        {
            //TODO : Handle error here
        }

        $params = [
                      'mode' => 'edit',
                      'blacklist' => $obj,
                  ];
        return(view('add_edit_blacklist', $params));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obj = $this->m;

        $fields = [
                      'pattern' => $request->input('pattern'),
                      'description' => $request->input('desc'),
                  ];

        $this->validateRequest('update', $id, $request);

        $obj->where('id', $id)
            ->update($fields);

        $res = $this->index($request);
        return($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Blacklist $blacklist)
    {
        $ids = $request->input('cbxDelete');
        $this->m->destroy($ids);

        //TODO : Need to check the number of element in array and reject if above the limit.

        $res = $this->index($request);
        return($res);
    }

    /**
     * Check if URL is in the blacklist.
     *
     * @param  string $url
     * @return boolean
     */
    public function isInBlacklist($url)
    {
        $blacklists = $this->m->all();

        //TODO : We might be able to use regular expression in the SQL where clause but not all database has that feature

        foreach ($blacklists as $bl)
        {
            $pattern = $bl->pattern;

            //First check it as string if it match exactly
            if ($pattern == $url)
            {
                return(true);
            }
        
            $preg = sprintf("#%s#", $pattern);
            preg_match($preg, $url, $matches);

            if (count($matches) > 0)
            {
                return(true);
            }
        }

        return(false);
    }

    /* == Private here == */

    private function validateRequest($type, $id, $request)
    { 
        $max = 255;

        $patternRule = "required|unique:blacklists|max:$max";
        if ($id > 0)
        {
            $patternRule =  "required|unique:blacklists,pattern,{$id}|max:$max";
        }

        //TODO : User our own error message rather than the default ones from Laravel
        $validator = Validator::make($request->all(),
            [
                'pattern' => $patternRule,
            ]);

        $validator->validate();

        //TODO : Validate for regx syntax
/*
        $validator = Validator::make($request->all(), 
            [
                'pattern' => [
                    function($attribute, $value, $fail) {
                        if (!@preg_match('/' . $value . '/', '')) {
                            return $fail('Invalid regular expression syntax!!!');
                        }
                    },
                ],
            ]);

        $validator->validate();
*/
    }
}
