<?php

namespace App\Http\Controllers;

use App\Url;
use App\Http\Controllers\BlacklistController;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UrlController extends Controller
{
    private const BASE_URL = 'https://sandbox.wintech-thai.com/laravel/shortener/public';
    private const MAX_SIZE = 250;
    private const ITEM_PER_PAGE = 7;

    private $m = null;
    private $b = null;

    public function __construct(Url $url, BlacklistController $blacklist)
    {
        $this->m = $url;
        $this->b = $blacklist;
    }

    /**
     * This method need authentication first
     */
    public function index(Request $request)
    {
        $base_url = env('BASE_URL', $this::BASE_URL);
        $search = $request->input('search_text');

        $m = $this->m->where('short_url', 'LIKE', '%'.$search.'%')
                     ->orWhere('long_url', 'LIKE', '%'.$search.'%')
                     ->orderBy('id', 'DESC');

        $urls = $m->paginate($this::ITEM_PER_PAGE);
        $this->scrubData($urls);

        $tot = $urls->total();
        $cnt = ceil($tot/$this::ITEM_PER_PAGE);
        $pageNo = $urls->currentPage();

        $params = [
                      'urls' => $urls, 
                      'page_count' => $cnt,
                      'total_record' => $tot,
                      'curr_page' => $pageNo,
                      'search_text' => $search,
                      'base_url' => $base_url,
                  ];
        return(view('tab_url', $params));
    }

    public function remove(Request $request)
    {
        $ids = $request->input('cbxDelete');
        $this->m->destroy($ids);

        //TODO : Need to check the number of element in array and reject if above the limit.

        $res = $this->index($request);
        return($res);
    }

    public function create()
    {
        $params = [
                      'error_flag' => false,
                      'error_msg' => '',
                      'shorten_url' => "",
                      'long_url' => "",
                  ];

        $this->populateDefaultParam($params);

        return(view('urlentry', $params));
    }

    /**
     * Shorten the URL.
     *
     * @param  string $url
     * @return \Illuminate\Http\Response
     */
    public function shorten(Request $request)
    {
        $base_url = env('BASE_URL', $this::BASE_URL);
        $url = $request->input('long_url');

        list($vw, $urlObj) = $this->validateRequest('shorten', '', $url);
        if ($vw != null)
        {
            return($vw);
        }
    
        $shorten_url = '';
        if ($url != '')
        {
            //Came from "POST" form submited

            $enc = $this->shortenUrl($url);
            $shorten_url = sprintf("%s/%s", $base_url, $enc);

            $urlObj = $this->getUrlHistory($enc);
            if ($urlObj == null)
            {
                //Not exist, then add the new one
                $this->addNewUrl($request, $enc);
            }

            //Do nothing if already exist
        }

        $params = [
                      'error_flag' => false,
                      'error_msg' => '',
                      'shorten_url' => "$shorten_url",
                      'long_url' => "$url",
                  ];
        $this->populateDefaultParam($params);

        return(view('urlentry', $params));
    }

    /**
     * Redirect page.
     *
     * @param  string $url
     * @return \Illuminate\Http\Response
     */
    public function redirect($key)
    {
        list($vw, $urlObj) = $this->validateRequest('redirect', $key, ''); 
        if ($vw != null)
        {
            return($vw);
        }

        $this->updateHitCount($key);
        $url = $urlObj->long_url; 

        return(redirect()->away($url, 302));
    }

    //=== Private functions here ===

    private function scrubData($urls)
    {
        //Added some logics here to manipulate the data

        foreach ($urls as $url)
        {
            $isExpire = $this->isDateExpire($url);
            $url->isExpire = $isExpire;

            $url->isInBlacklist = $this->b->isInBlacklist($url);
        }
    }

    private function validateRequest($type, $key, $url)
    {
        //TODO : Refactor to use Validator and MessageBag here

        $params = [
                      'error_flag' => false,
                      'error_msg' => '',
                      'shorten_url' => '',
                      'long_url' => '',
                  ];
        $this->populateDefaultParam($params);

        $urlObj = null;

        if ($type == 'redirect')
        {
            $urlObj = $this->getUrlHistory($key);

            if ($urlObj == null)
            {
                $message = 'Shorten URL not found !!!!';
                $resp = response($message, 410);

                return([$resp, null]);
            }

            if ($this->isDateExpire($urlObj))
            {
                // Let go to URL entry page to allow user add the new one 
                $params['error_flag'] = true;
                $params['error_msg'] = 'URL is expire !!!!';

                $vw = view('urlentry', $params);
                return([$vw, null]);
            }

            $url = $urlObj->long_url;
        }
        else if ($type == 'shorten')
        {
            $size = strlen($url);
            $pattern = '%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu';

            if ($size > $this::MAX_SIZE)
            {
                $params['error_flag'] = true;
                $params['error_msg'] = 'URL is too long !!!!';

                $vw = view('urlentry', $params);
                return([$vw, null]);
            }
            else if (!preg_match("$pattern", $url))
            {
                $params['error_flag'] = true;
                $params['error_msg'] = 'Not a valid URL !!!!';

                $vw = view('urlentry', $params);
                return([$vw, null]);
            } 
        }

        //Check if URL is baned here

        if ($this->b->isInBlacklist($url))
        {
            $params['error_flag'] = true;
            $params['error_msg'] = 'URL is in the blacklist !!!!';

            $vw = view('urlentry', $params);
            return([$vw, null]);

            return([$resp, null]);
        }

        return([null, $urlObj]);
    }

    private function isDateExpire($urlObj)
    {
        $currDate = Carbon::now();
        $expireDate = $urlObj->expire_date;

        $result = isset($expireDate) && $currDate->gt($expireDate);
        return($result);
    }

    private function populateDefaultParam(& $params)
    {
    }

    /**
     * Simplify long URL to shorten form.
     *
     * @param  string $url
     * @return string 
     */
    private function shortenUrl($url)
    {
        //CREDIT - https://stackoverflow.com/questions/3514057/php-url-shortening-algorithm

        /*
         * Actually, we can use the sequence or max ID from database but we have to be aware of 
         * the number collision, for example, user call to the service in the same time.
         * Yeah, this is race condition. 
        */

        // Let use MD5 for now for simplicity
        $shorten = hash_hmac('md5', "$url", "secretkey");
        return($shorten);
    }

    /**
     * Get the URL match.
     *
     * @param  string $short_url
     * @return object 
     */
    private function getUrlHistory($key)
    {
        //If exist, it suppose to be only 1 record returned

        $urlObj = $this->m->where('short_url', $key)->first();

        return($urlObj);
    }

    /**
     * Add url to table 
     *
     * @param  string $long_url
     * @return void 
     */
    private function addNewUrl($request, $key)
    {
        $url = $request->input('long_url');

        //Convert from string to int. Do not trust input received
        $dayCnt = intval($request->input('optDay'));

        $currDtm = Carbon::now();
        $expireDate = null;

        if ($dayCnt > 0)
        {
            $expireDate = $currDtm->addDays($dayCnt);
        }

        $urlObj = $this->m;

        $fields = [
                      'long_url' => $url,
                      'short_url' => $key,
                      'hit_count' => 0,
                      'expire_date' => $expireDate,
                  ];
          
        $urlObj->create($fields);
    }

    /**
     * Increment the hit count 
     *
     * @param  \App\User $urlObj
     * @return void 
     */
    private function updateHitCount($key)
    {
        //TODO : We will have to see why hit count is updated twice

        $urlObj = $this->getUrlHistory($key);
        if ($urlObj == null)
        {
            //Do Nothing
            return;
        }

        $cnt = $urlObj->hit_count;
        $cnt++;

        $urlObj->hit_count = $cnt;

        //Update the existing one here
        $urlObj->save();
    }
}
