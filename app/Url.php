<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = [
                  'long_url',
                  'short_url',
                  'hit_count',
                  'expire_date',
              ];

    protected $isExpire = false;
    protected $isInBlacklist = false;
}


